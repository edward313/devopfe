import React from "react";
import { useState, useEffect } from "react";
import { useRef } from "react";
const Hero = () => {
	const [data, setData] = useState(null);
	const initialized = useRef(false);
	useEffect(() => {
		if (!initialized.current) {
			initialized.current = true;
			fetch("https://api.phatluu.website/api/container", {
				method: "GET",
			})
				.then((response) => response.json())
				.then((data) => {
					setData(data.data);
				})
				.catch((error) => console.log(error));
		}
	}, []);

	return (
		<section class="mb-40">
			<div class="bg-blue-400 py-24 px-6 text-center dark:bg-neutral-900">
				<h1 class="mt-2 mb-16 text-5xl font-bold tracking-tight md:text-6xl xl:text-7xl">
					2023-6-12  <br />
					<span class="text-primary">{data}</span>
				</h1>
			</div>
		</section>
	);
};

export default Hero;
